const Customer = require("../models/customer.model");

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty !",
        });
    }

    const customer = new Customer({
        CustomerName: req.body.CustomerName,
        Email: req.body.Email,
        Active: req.body.Active,
    });

    Customer.create(customer, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occured.",
            });
        else res.send(data);
    });
};

exports.findAll = (req, res) => {
    Customer.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occured.",
            });
        else res.send(data);
    });
};

exports.findOne = (req, res) => {
    Customer.findById(req.params.customerId, (err, data) => {
        if (err) {
            if (err.kind === "not found") {
                res.status(404).send({
                    message: `Not found customer with id ${req.params.customerId}.`,
                });
            } else {
                res.status(500).send({
                    message:
                        "Error retrieving customer with id " +
                        req.params.customerId,
                });
            }
        } else res.send(data);
    });
};

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty.",
        });
    }

    Customer.updateById(
        req.params.customerId,
        new Customer(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not found") {
                    res.status(404).send({
                        message: `Not found customer with id ${req.params.customerId}.`,
                    });
                } else {
                    res.status(500).send({
                        message:
                            "Error updating customer with id " +
                            req.params.customerId,
                    });
                }
            } else res.send(data);
        }
    );
};

exports.delete = (req, res) => {
    Customer.remove(req.params.customerId, (err, data) => {
        if (err) {
            if (err.kind === "not found") {
                res.status(404).send({
                    message: `Not found customer with id ${req.params.customerId}.`,
                });
            } else {
                res.status(500).send({
                    message:
                        "Could not delete customer with id " +
                        req.params.customerId,
                });
            }
        } else res.send({ message: `Customer was deleted succesfully !` });
    });
};

exports.deleteAll = (req, res) => {
    Customer.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occured while removing all customers.",
            });
        else res.send({ message: `All customers were deleted successfully!` });
    });
};
