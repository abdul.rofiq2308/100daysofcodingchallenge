const sql = require("./db.js");

const Customer = function (customer) {
    this.CustomerName = customer.CustomerName;
    this.Email = customer.Email;
    this.Active = customer.Active;
    this.CreateBy = customer.CreateBy;
    this.CreateDt = customer.CreateDt;
    this.UpdateBy = customer.UpdateBy;
    this.UpdateDt = customer.UpdateDt;
};

Customer.create = (newCustomer, result) => {
    sql.query("INSERT INTO tblcustomer SET ?", newCustomer, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("create customer: ", { id: res.insertId, ...newCustomer });
        result(null, { id: res.insertId, ...newCustomer });
    });
};

Customer.findById = (customerId, result) => {
    sql.query(
        `SELECT * FROM tblcustomer WHERE id = ${customerId}`,
        (err, res) => {
            if (err) {
                console.log("error", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found customer: ", res[0]);
                result(null, res[0]);
                return;
            }

            result({ kind: "not found" }, null);
        }
    );
};

Customer.getAll = (result) => {
    sql.query("SELECT * FROM tblcustomer", (err, res) => {
        if (err) {
            console.log("error", res);
            result(null, err);
            return;
        }

        console.log("customers ", res);
        result(null, res);
    });
};

Customer.updateById = (id, customer, result) => {
    sql.query(
        "UPDATe tblcustomer SET CustomerName = ?, Email = ?, Active = ? WHERE id = ?",
        [customer.CustomerName, customer.Email, customer.Active, id],
        (err, res) => {
            if (err) {
                console.log("error :", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({ kind: "not found" }, null);
                return;
            }

            console.log("update customer : ", { id: id, ...customer });
            result(null, { id: id, ...customer });
        }
    );
};

Customer.remove = (id, result) => {
    sql.query("DELETE FROM tblcustomer WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error :", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            result({ kind: "not found" }, null);
            return;
        }

        console.log("deleted customer with id :", id);
        result(null, res);
    });
};

Customer.removeAll = (result) => {
    sql.query("DELETE FROM tblcustomer", (err, res) => {
        if (err) {
            console.log("error ", err);
            result(null, err);
            return;
        }

        console.log(`delete ${res.affectedRows} customers`);
        result(null, res);
    });
};

module.exports = Customer;
